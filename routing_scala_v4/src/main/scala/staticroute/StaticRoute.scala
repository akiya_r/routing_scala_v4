package staticroute

import akka.actor.ActorRef
import routegraph.RouteGraph

import scala.collection.mutable


class StaticRoute(_nDC: Int, _nGC: Int, rg: RouteGraph) {
  val route_graph = rg
  val nDC = _nDC
  val nGC = _nGC

  val static_route = mutable.ArrayBuffer[List[String]]()
  val assignment_connecting_node = mutable.ArrayBuffer[String]()
  val assignment_nDC = mutable.ArrayBuffer[Int]()
  val assignment_DC = mutable.ArrayBuffer[ActorRef]()
}