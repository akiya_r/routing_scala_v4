package cabin

import akka.actor.Actor.Receive
import akka.actor._
import message._

import scala.collection.mutable


class GuestCabin(initial_node: ActorRef) extends Actor {
  import MsgToGuestCabin._

  var label = ""
  var connection: Option[ActorRef] = None
  var node: Option[ActorRef] = Some(initial_node)

  println(s"      [${self.path.name}] Boot At ${initial_node.path.name}") // debug_code
  node.get ! MsgToConnectingNodeServer.BootGuestCabin(self)


  override def receive: Receive = {
    case SetLabel(_label) => {
      label = _label
      connection.get ! MsgToMainServer.CabinChangeLabel(self, _label)
      connection.get ! MsgToDriverCabin.GuestCabinIsReady()
    }

    case ConnectTo(dc) => connection match {
      case None => {
        connection = Some(dc)
        connection.get ! MsgToDriverCabin.GuestCabinIsReady()
      }
      case Some(_) => {
        println(s"      [${self.path.name}] I am connecting to ${connection.get.path.name}, but I get ConnectTo from ${sender.path.name}")
        // do nothing
      }
    }

    case Disconnect() => {
      connection.get ! MsgToDriverCabin.GuestCabinIsReady()
      connection = None
    }

    case GoTo(_node) => {
      node = None
      // do nothing
    }

    case ArriveAt(_node) => {
      connection.get ! MsgToDriverCabin.GuestCabinIsReady()
    }

    case ArriveAtConnectingNode(_node) => {
      node = Some(_node)
    }
  }
}
