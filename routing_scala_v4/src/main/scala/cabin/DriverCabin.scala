package cabin

import akka.actor._
import message._
import routegraph.RouteGraph

import scala.collection.mutable

class DriverCabin(initial_node: ActorRef, _route_graph: RouteGraph) extends Actor {

  import MsgToDriverCabin._

  val route_graph = _route_graph
  // driver cabin status
  var label = ""
  // the guest cabins connecting
  val connecting_guest_cabins = mutable.Set[ActorRef]()
  var ready_guest_cabin = 0
  var connecting_node: Option[ActorRef] = Some(initial_node)
  // the buffer of commands
  val commands = mutable.ListBuffer[Any]()

  var prev = initial_node.path.name
  var next: Option[String] = None

  //  println(s"    [${self.path.name}] Boot At ${initial_node.path.name}") // debug_code
  initial_node ! MsgToConnectingNodeServer.BootDriverCabin(self)


  override def receive: Receive = {
    case Commands(cmds) => {
      println(s"    [${self.path.name}] " + cmds)
      println(s"    [${self.path.name}] Get Commands")
      commands.clear()
      commands ++= cmds
      exec_next_command()
    }

    case ConnectGuestCabins(gcs) => {
      ready_guest_cabin = 0
      connecting_guest_cabins ++= gcs

      connecting_guest_cabins foreach {
        _ ! MsgToGuestCabin.ConnectTo(self)
      }

      println(s"    [${self.path.name}] connect guest cabin (${
        this.connecting_guest_cabins map {
          _.path.name
        }
      })")

      if (connecting_guest_cabins.isEmpty) exec_next_command()
    }

    case DisconnectGuestCabins() => {
      println(s"    [${self.path.name}] disconnect guest cabin (${
        connecting_guest_cabins map {
          _.path.name
        }
      })")
      ready_guest_cabin = 0
      connecting_guest_cabins foreach {
        _ ! MsgToGuestCabin.Disconnect()
      }

      if (connecting_guest_cabins.isEmpty) exec_next_command()
    }

    case GoTo(node) => {
      //      println(s"    [${self.path.name}] go to $node")
      next = Some(node)
      ready_guest_cabin = 0
      // inform gcs where we go
      connecting_guest_cabins foreach {
        _ ! MsgToGuestCabin.GoTo(next.get)
      }
      // moving now...
      Thread.sleep(route_graph.get_weight(prev, next.get).toLong)
      println(s"    [${self.path.name}] arrive at ${next.get}")
      // inform gcs where we arrive
      connecting_guest_cabins foreach {
        _ ! MsgToGuestCabin.ArriveAt(next.get)
      }
      prev = next.get
      next = None

      if (connecting_guest_cabins.isEmpty) exec_next_command()
    }

    case InformArrivingAtConnectingNode(node) => {
      println(s"    [${self.path.name}] inform ${node.path.name} that we arrive")
      connecting_guest_cabins foreach {
        _ ! MsgToGuestCabin.ArriveAtConnectingNode(node)
      }
      node ! MsgToConnectingNodeServer.DriverCabinArrive(self, connecting_guest_cabins.toSet)
      connecting_guest_cabins.clear()
      connecting_node = Some(node)
      if (connecting_guest_cabins.isEmpty) exec_next_command()
    }

    case InformLeavingAtConnectingNode(node) => {
      println(s"    [${self.path.name}] inform ${node.path.name} that we leave there")
      node ! MsgToConnectingNodeServer.DriverCabinLeaveWith(self, connecting_guest_cabins.toSet)
      exec_next_command()
    }

    case Wait(millisec) => {
      println(s"    [${self.path.name}] waiting now...")
      Thread.sleep(millisec)
      exec_next_command()
    }

    case GuestCabinIsReady() => {
      println(s"    [${self.path.name}] ${sender.path.name} is ready")
      ready_guest_cabin += 1
      if (ready_guest_cabin == connecting_guest_cabins.size) {
        ready_guest_cabin = 0
        exec_next_command()
      }
    }

    case SetLabel(_label, cabin) => {
      if (cabin == self) {
        this.label = _label
        connecting_node.get ! MsgToMainServer.CabinChangeLabel(self, _label)
        exec_next_command()
      } else if (connecting_guest_cabins contains cabin) {
        cabin ! MsgToGuestCabin.SetLabel(_label)
      }
      if (connecting_guest_cabins.isEmpty) exec_next_command()

    }

    case MsgToMainServer.CabinChangeLabel(cabin, new_label) => {
      connecting_node.get ! MsgToMainServer.CabinChangeLabel(cabin, new_label)
    }
  }

  private def exec_next_command() = {
    if (commands.nonEmpty) {
      val cmd = commands.head
      //      println(s"    [${self.path.name}] $cmd")
      commands.remove(0)
      self ! cmd
    } else {
      // do nothing
    }
  }
}
