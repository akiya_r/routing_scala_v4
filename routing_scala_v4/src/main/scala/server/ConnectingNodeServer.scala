package server

import akka.actor.Actor.Receive
import akka.actor._
import akka.pattern.ask
import akka.util.Timeout
import scala.concurrent.{Await, Future}
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._
import scala.language.postfixOps
import scala.util.{Failure, Success, Try}
import message._
import routegraph.RouteGraph

import scala.collection.JavaConverters._
import scala.collection.mutable

class ConnectingNodeServer(_route_graph: RouteGraph) extends Actor {

  import MsgToConnectingNodeServer._

  implicit val timeout = Timeout(5.seconds)

  var main_server: Option[ActorRef] = None
  var main_server_die = true
  val cardiophone = context.actorOf(Props(classOf[Cardiophone]), "cardiophone")
  val driver_cabin_pool = mutable.Set[ActorRef]()
  val guest_cabin_pool = mutable.Set[ActorRef]()
  val time_driver_cabin_arrived = mutable.Map[ActorRef, Long]()
  val time_guest_cabin_arrived = mutable.Map[ActorRef, Long]()
  val route_graph = _route_graph
  val cabin_commands = mutable.Map[ActorRef, List[Any]]()
  val driver_cabin_route = mutable.Map[ActorRef, List[String]]()

  val static_route = mutable.ArrayBuffer[List[String]]()
  val nCabin_static_route = mutable.ArrayBuffer[Int]()
  val route_nCabin = mutable.Map[List[String], (Int, Int)]()
  val route_dc = mutable.Map[Int, Array[ActorRef]]()

  override def receive: Receive = {
    case CommandsTo(cabin, commands) => {
      cabin_commands(cabin) = commands
      val route = commands2route(cabin_commands(cabin))
      driver_cabin_route += (cabin -> route)
      val gcs = gcs_in_commands(cabin_commands(cabin))

      if ((driver_cabin_pool contains cabin) && gcs.nonEmpty && (gcs subsetOf guest_cabin_pool)) {
        println(s"  [${self.path.name}] Get Commands and forward to ${cabin.path.name}")
        cabin ! MsgToDriverCabin.Commands(cabin_commands(cabin))
        cabin_commands(cabin) = List[Any]()
      } else {
        println(s"  [${self.path.name}] Get Commands")
        cabin_commands.clear()
        cabin_commands(cabin) = commands
      }
    }

    case StaticSchedule(_route_nCabin) => {
      route_nCabin ++= _route_nCabin
    }

    case BootDriverCabin(dc) => {
      driver_cabin_pool += dc
      main_server match {
        case None => {
          // do nothing currently
        }
        case Some(ms) => {
          ms ! MsgToMainServer.BootDriverCabinAt(dc, self)
        }
      }
      println(s"  [${self.path.name}] Boot ${dc.path.name}")
    }

    case BootGuestCabin(gc) => {
      guest_cabin_pool += gc
      main_server match {
        case None => {
          // do nothing currently
        }
        case Some(ms) => {
          ms ! MsgToMainServer.BootGuestCabinAt(gc, self)
          println(s"  [${self.path.name}] Boot ${gc.path.name}")
        }
      }
    }

    case DriverCabinArrive(dc, gcs) => {
      driver_cabin_pool += dc
      guest_cabin_pool ++= gcs

      main_server match {
        case None => {
          // do nothing currently
        }
        case Some(ms) => {
          ms ! MsgToMainServer.CabinsArriveAt(dc, gcs, self)

        }
      }
      // give dc commands
      for ((dc, commands) <- cabin_commands) {
        val gcs = gcs_in_commands(commands)
        if ((driver_cabin_pool contains dc) && gcs.nonEmpty && (gcs subsetOf guest_cabin_pool)) {
          dc ! MsgToDriverCabin.Commands(commands)
          cabin_commands remove dc
        }
      }
    }

    case DriverCabinLeaveWith(dc, gcs) => {
      driver_cabin_pool -= dc
      guest_cabin_pool --= gcs
      main_server match {
        case None => {
          // do nothing currently
        }
        case Some(ms) => {
          ms ! MsgToMainServer.DriverCabinLeaveAndGoTo(dc, gcs, self, driver_cabin_route(dc))
        }
      }
    }


    case MainServerDie() => {
      println(s"  [${self.path.name}] I get to know that the main server get to die.")
      //      main_server = None
      main_server_die = true
    }

    case MainServerReborn(new_main_server) => {
      println(s"  [${self.path.name}] I get to know that the main server get to live")

      main_server = Some(new_main_server)
      main_server_die = false
      main_server.get ! MsgToMainServer.BootConnectingNodeServer(self)
      for (dc <- driver_cabin_pool) {
        main_server.get ! MsgToMainServer.BootDriverCabinAt(dc, self)
      }
      for (gc <- guest_cabin_pool) {
        main_server.get ! MsgToMainServer.BootGuestCabinAt(gc, self)
      }
    }

    case MsgToMainServer.CabinChangeLabel(dc, new_label) => {
      main_server match {
        case None => {
          // do nothing currently
        }
        case Some(ms) => {
          ms ! MsgToMainServer.CabinChangeLabel(dc, new_label)
        }
      }
    }

    case msg => println(s"[${self.path.name}] unknown message: " + msg)
  }

  private def commands2route(commands: List[Any]): List[String] = {
    val route = mutable.ListBuffer[String]()

    commands foreach {
      case MsgToDriverCabin.GoTo(node) => route += node
      case _ => ; // do nothing
    }

    println(s"  [${
      self.path.name
    }] commands2route $route")
    route.toList
  }

  private def gcs_in_commands(commands: List[Any]) = {
    val  connection_commands= commands.filter {
      _.isInstanceOf[MsgToDriverCabin.ConnectGuestCabins]
    }
    if (connection_commands.nonEmpty) {
      connection_commands.head.asInstanceOf[MsgToDriverCabin.ConnectGuestCabins].gcs
    } else {
      Set[ActorRef]()
    }
  }
}


class Cardiophone extends Actor {

  import MsgToCardiophone._

  var main_server_heart: Option[ActorRef] = None
  var main_server: Option[ActorRef] = None
  val connecting_node = context.parent
  implicit val timeout = Timeout(10.seconds)


  self ! FindMainServer()

  override def receive: Receive = {
    case NextCheck() => {
      val heart_beat_future = main_server_heart.get ? MsgToHeart.Listen()
      //      heart_beat onComplete {
      //        case Success(beat) => beat match {
      //          case HeartBeat() => {
      //            Thread.sleep(1000)
      //            self ! NextCheck()
      //            //            println(s"  [${connecting_node.path.name}] I listened to heart beat")
      //          }
      //          case _ => {
      //            println(s"  [${connecting_node.path.name}] I could NOT listened to heart beat")
      //            connecting_node ! MsgToConnectingNodeServer.MainServerDie()
      //            self ! FindMainServer()
      //          }
      //        }
      //        case Failure(ex) => {
      //          println(s"  [${connecting_node.path.name}] I could NOT listened to heart beat")
      //          connecting_node ! MsgToConnectingNodeServer.MainServerDie()
      //          self ! FindMainServer()
      //        }
      //      }
      try {
        val heart_beat = Await.result(heart_beat_future, timeout.duration)
        heart_beat match {
          case HeartBeat() => {
            Thread.sleep(5000)
            self ! NextCheck()
          }
          case NoHeartBeat() => {
            connecting_node ! MsgToConnectingNodeServer.MainServerDie()
            self ! NextCheck()
          }
        }

      } catch {
        case e: Throwable => {
          println(e)
          connecting_node ! MsgToConnectingNodeServer.MainServerDie()
          self ! FindMainServer()
        }
      }
    }

    case FindMainServer() => {
      val future = context.actorSelection("../../main_server").resolveOne()
      future onComplete {
        case Success(actor_ref) => actor_ref match {
          case actor_ref: ActorRef if actor_ref.path.name == "main_server" => {
            val ms = actor_ref
            // get main server heart beat
            val future1 = ms ? MsgToMainServer.GetHeart()
            future1 onComplete {
              case Success(heart) => {
                main_server_heart = Some(heart.asInstanceOf[ActorRef])
                main_server = Some(ms)
                connecting_node ! MsgToConnectingNodeServer.MainServerReborn(ms)
                self ! NextCheck()
              }
              case Failure(ex) => {
                self ! FindMainServer()
              }
            }

          }
          case _ => {
            self ! FindMainServer()
          }
        }

        case Failure(ex) => {
          self ! FindMainServer()
        }
      }

    }
  }
}