package server

import akka.actor._
import akka.pattern.ask
import akka.util.Timeout

import scala.concurrent.{Await, Future}
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._
import scala.language.postfixOps
import scala.util.{Failure, Random, Success, Try}
import scala.collection.mutable
import routegraph.RouteGraph
import message.{MsgToDriverCabin, _}


class MainServer(_nDC: Int, _nGC: Int, _route_graph: RouteGraph) extends Actor {

  import MsgToMainServer._

  implicit val timeout = Timeout(5.seconds)

  // status
  val nDC = _nDC
  val nGC = _nGC
  val iDC = 0
  val iGC = 0
  var route_graph = _route_graph
  var main_server_die = false

  // actor reference
  val heart = context.actorOf(Props(classOf[MainServerHeart]), "heart")
  val driver_cabin = mutable.Map[String, ActorRef]()
  val guest_cabin = mutable.Map[String, ActorRef]()
  val connecting_node_servers = mutable.Map[String, ActorRef]()
  val scheduler = context.actorOf(Props(classOf[Scheduler], route_graph), "scheduler")

  // driver cabin status
  val dc_route = mutable.Map[ActorRef, List[String]]()

  // guest cabin status
  val guest_cabin_connection = mutable.Map[ActorRef, Option[ActorRef]]()

  // request
  val requests = mutable.Set[Request]()


  scheduler ! MsgToScheduler.Data(route_graph, driver_cabin.toMap, guest_cabin.toMap, connecting_node_servers.toMap)

  // debug_code
  println("Boot Main Server")

  override def receive: Receive = {
    case request: Request => {
      requests += request
    }

    case BootConnectingNodeServer(node) => {
      val node_name = node.path.name
      println(s"[Main Server] Boot CN($node_name)")
      connecting_node_servers += (node_name -> node)
    }

    case BootDriverCabinAt(dc, node) => {
      val dc_name = dc.path.name
      println(s"[Main Server] Boot DC($dc_name) at ${node.path.name}")
      driver_cabin += (dc_name -> dc)
      route_graph.set_driver_cabin_location(dc, node.path.name)
      route_graph.set_dc_destination(dc, node.path.name)
      dc_route += (dc -> List[String]())
      route_graph.set_time_driver_cabin_arrival(dc, System.currentTimeMillis())
    }

    case BootGuestCabinAt(gc, node) => {
      println(s"[Main Server] Boot GC(${gc.path.name}) at ${node.path.name}")
      guest_cabin += (gc.path.name -> gc)
      route_graph.set_guest_cabin_location(gc, node.path.name)
      route_graph.set_gc_destination(gc, node.path.name)
      guest_cabin_connection += (gc -> None)
      route_graph.set_time_guest_cabin_arrival(gc, System.currentTimeMillis())
    }

    case CabinsArriveAt(dc, gcs, node) => {
      println(s"[Main Server] ${dc.path.name}::{${
        gcs map {
          _.path.name
        }
      }} arrive at ${node.path.name}")

      val time_arrival = System.currentTimeMillis()

      dc_route(dc) = List[String]()
      route_graph.set_driver_cabin_location(dc, node.path.name)
      route_graph.set_time_driver_cabin_arrival(dc, time_arrival)
      gcs foreach { gc: ActorRef =>
        guest_cabin_connection(gc) = None
        route_graph.set_guest_cabin_location(gc, node.path.name)
        route_graph.set_time_guest_cabin_arrival(gc, time_arrival)
      }
    }

    case DriverCabinLeaveAndGoTo(dc, gcs, node, route) => {
      println(s"[Main Server] ${dc.path.name}::{${
        gcs map {
          _.path.name
        }
      }} leave ${node.path.name}")
      route_graph.set_dc_destination(dc, route.last)
      dc_route(dc) = List(route_graph.get_driver_cabin_location(dc)) ++ route
      route_graph.set_driver_cabin_location(dc, "")
      gcs foreach { gc: ActorRef =>
        guest_cabin_connection(gc) = Some(dc)
        route_graph.set_guest_cabin_location(gc, "")
        route_graph.set_gc_destination(gc, route.last)
      }
      route_graph.set_time_driver_cabin_departure(dc, System.currentTimeMillis())
    }

    case CabinChangeLabel(cabin, new_label) => {
      //      cabin_label += (cabin -> new_label)
      route_graph.set_cabin_label(cabin, new_label)
    }

    case GetData() => {
      scheduler ! MsgToScheduler.Data(route_graph, driver_cabin.toMap, guest_cabin.toMap, connecting_node_servers.toMap)
    }

    case SchedulingResult(cabin_commands) => {
      for ((dc, commands) <- cabin_commands) {
        println(s"[Main Server] send commands to " + route_graph.get_dc_destination(dc))
        connecting_node_servers(route_graph.get_dc_destination(dc)) ! MsgToConnectingNodeServer.CommandsTo(dc, commands)
      }
    }

    case GetDriverCabinLocations() => {
      val cabin_location = mutable.Map[String, MsgToGraphics.Location]()

      for ((_, dc_ref) <- driver_cabin) {
        val (prev, next, prog) = get_location(dc_ref)
        val location = MsgToGraphics.Location(prev, next, prog)
        cabin_location += (dc_ref.path.name -> location)
      }

      sender() ! MsgToGraphics.Locations(cabin_location.toMap)
    }

    case GetGuestCabinLocations() => {
      val connection_cabin = mutable.Map[String, Set[String]]()

      // init connection_cabin
      for (connection <- driver_cabin.keys.toSet ++ connecting_node_servers.keys.toSet) {
        connection_cabin += (connection -> Set[String]())
      }

      // set connection
      for ((name, gc_ref) <- guest_cabin) {
        if (guest_cabin_connection(gc_ref).nonEmpty) {
          connection_cabin(guest_cabin_connection(gc_ref).get.path.name) += name
        } else {
          val prev = route_graph.get_guest_cabin_location(gc_ref)
          connection_cabin(prev) += name
        }
      }

      sender ! MsgToGraphics.Connections(connection_cabin.toMap)
    }

    case GetLabels() => {
      //      val cabin_label = (for ((cabin_ref, label) <- this.cabin_label) yield (cabin_ref.path.name, label)).toMap
      val cabin_label = (
        for ((cabin_ref, label) <- route_graph.get_cabin_labels())
          yield (cabin_ref.path.name, label)
        ).toMap

      //      println(s"[Main Server] send $cabin_label to Graphics")

      sender ! cabin_label
    }

    case GetRequests() => {
      val node_user = mutable.Map[String, Int]()

      for (request <- requests) {
        val nPeople = request.nPeople + node_user.getOrElse(request.departure, 0)
        node_user += (request.departure -> nPeople)
      }

      sender ! node_user.toMap
    }

    case GetHeart() => {
      sender ! heart
    }

    case Die() => {
      if (!main_server_die) {
        scheduler ! MsgToScheduler.Stop()
        heart ! MsgToHeart.Stop()
      }
    }

    case Reborn() => {
      if (main_server_die) {
        heart ! MsgToHeart.Reborn()
        scheduler ! MsgToScheduler.Data(route_graph, driver_cabin.toMap,
          guest_cabin.toMap, connecting_node_servers.toMap)
      }
    }
    case msg => println("[Main Server] unknown message: " + msg)
  }


  def get_location(cabin: ActorRef): (String, Option[String], Double) = {
    if (driver_cabin contains cabin.path.name) {
      if (dc_route(cabin).nonEmpty) {
        val route = dc_route(cabin)
        var prev = dc_route(cabin).head
        val time_now = System.currentTimeMillis()
        val time_from_departure = time_now - route_graph.get_time_driver_cabin_departure(cabin)
        var sum_time: Long = route_graph.get_weight(prev, route(1)).toLong
        var i = 1
        while (sum_time < time_from_departure) {
          prev = route(i)
          i += 1
          if (i < route.size) {
            sum_time += route_graph.get_weight(prev, route(i)).toLong
          } else {
            sum_time = time_from_departure + 1
          }
        }
        val (progress, next) = if (i < route.size) {
          (1 - (sum_time - time_from_departure).toDouble / route_graph.get_weight(prev, route(i)), Some(route(i)))
        } else {
          (1.0, Some(route(i - 1)))
        }
        (prev, next, progress)
      } else {
        (route_graph.get_driver_cabin_location(cabin), None, 0.0)
      }
    } else if ((guest_cabin contains cabin.path.name) && guest_cabin_connection(key = cabin).isDefined) {
      val dc = guest_cabin_connection(cabin).get
      if (dc_route(dc).nonEmpty) {
        val route = dc_route(dc)
        var prev = dc_route(dc).head
        val time_now = System.currentTimeMillis()
        val time_from_departure = time_now - route_graph.get_time_driver_cabin_departure(dc)
        var sum_time: Long = route_graph.get_weight(prev, route(1)).toLong
        var i = 1
        while (sum_time < time_from_departure) {
          prev = route(i)
          i += 1
          if (i < route.size) {
            sum_time += route_graph.get_weight(prev, route(i)).toLong
          } else {
            sum_time = time_from_departure + 1
          }
        }
        val (progress, next) = if (i < route.size) {
          (1 - (sum_time - time_from_departure).toDouble / route_graph.get_weight(prev, route(i)), Some(route(i)))
        } else {
          (1.0, Some(route(i - 1)))
        }
        (prev, next, progress)
      } else {
        (route_graph.get_driver_cabin_location(cabin), None, 0.0)
      }
    } else if (guest_cabin contains cabin.path.name) {
      val node: String = route_graph.get_guest_cabin_location(cabin)
      (node, None, 0.0)
    } else {
      ("", None, -1.0)
    }
  }
}


class Scheduler(_rg: RouteGraph) extends Actor {

  import message.MsgToScheduler._

  implicit val timeout = Timeout(5.seconds)
  val rand = new Random
  rand.setSeed(197)

  val main_server = context.parent
  val connecting_node = mutable.Map[String, ActorRef]()

  var route_graph = _rg
  var command_switch = true
  val dcs = mutable.Map[String, ActorRef]()
  val gcs = mutable.Map[String, ActorRef]()


  override def receive: Receive = {
    case Data(_route_graph, _dcs, _gcs, c_node) => {
      //      println(" [Scheduler] have got data")
      route_graph = _route_graph
      dcs ++= _dcs
      gcs ++= _gcs
      connecting_node ++= c_node
      update_route_graph()
      self ! Scheduling()
    }

    case CollectData() => {
      //      println(" [Scheduler] Getting data")
      main_server ! MsgToMainServer.GetData()
    }

    case Scheduling() => {
      println(" [Scheduler] Start scheduling")
      if (connecting_node.nonEmpty)
        scheduling()
      println(" [Scheduler] Finish scheduling")
      Thread.sleep(5000)
      self ! CollectData()
    }

    case Stop() => {
      // do nothing
    }
  }

  def scheduling() = {
    def area2route(begin: String, area: String, finish: Set[String] = Set()): Set[List[String]] = {
      val node_in_area = route_graph.get_nodes_of_area(area) -- finish - begin
      val neighbors = route_graph.get_neighbors(begin) & node_in_area
      val paths = mutable.Set[List[String]]()
      if (neighbors.nonEmpty) {
        for (neighbor <- neighbors) {
          paths ++= area2route(neighbor, area, finish + begin)
        }


        val copy_paths = paths.clone()
        for (path <- copy_paths) {
          paths -= path
          val add_path = List(begin) ++ path
          paths += add_path
        }
        for (path <- paths) {
          if (finish.isEmpty && !(connecting_node contains path.last)) {
            paths -= path
            paths += path ++ path.reverse.slice(1, path.size)
          }
        }
        paths.toSet
      } else {
        Set(List(begin))
      }
    }

    val dc_route = mutable.Map[ActorRef, List[String]]()
    val dc_gcs = mutable.Map[ActorRef, Set[ActorRef]]()
    val cabin_label = mutable.Map[ActorRef, String]()
    val dc_commands = mutable.Map[ActorRef, List[Any]]()

    for (dc <- dcs.values) {
      val loc_now = route_graph.get_driver_cabin_location(dc)
      val area_now = route_graph.get_area_of_node(loc_now)
      val neighbors = route_graph.get_neighbors(loc_now)
      val areas = (for (neighbor <- neighbors) yield route_graph.get_area_of_node(neighbor)).toList.flatten
      val area = areas(rand.nextInt(areas.size))
      val routes = area2route(loc_now, area).toList
      val route = routes(rand.nextInt(routes.size))

      dc_route += (dc -> route)
      dc_gcs += (dc -> Set[ActorRef]())
    }

    for (gc <- gcs.values) {
      val loc_now = route_graph.get_guest_cabin_location(gc)
      val dc_in_same_loc = route_graph.get_driver_cabin_in_location(loc_now).toList
      if (dc_in_same_loc.nonEmpty) {
        val dc = dc_in_same_loc(rand.nextInt(dc_in_same_loc.size))
        if (rand.nextDouble() < 1.0 / (dc_in_same_loc.size + 1)) {
          // do nothing
        } else {
          if (dc_gcs(dc).size < 3)
          dc_gcs(dc) = dc_gcs(dc) + gc
        }
      }
    }

    // transform to commands
    for (dc <- dcs.values) {
      dc_commands += (dc -> route2commands(dc, dc_gcs(dc), cabin_label.toMap, dc_route(dc)))
    }
    main_server ! MsgToMainServer.SchedulingResult(dc_commands.toMap)
  }

  private def route2commands(dc: ActorRef, gcs: Set[ActorRef],
                             cabin_label: Map[ActorRef, String], route: List[String]) = {
    val commands = mutable.ListBuffer[Any]()

    commands += MsgToDriverCabin.ConnectGuestCabins(gcs)
    for ((cabin, label) <- cabin_label) {
      if (dc == cabin || (gcs contains cabin))
        commands += MsgToDriverCabin.SetLabel(label, cabin)
    }
    commands += MsgToDriverCabin.InformLeavingAtConnectingNode(connecting_node(route.head))
    for (node <- route.slice(1, route.size)) commands += MsgToDriverCabin.GoTo(node)
    commands += MsgToDriverCabin.DisconnectGuestCabins()
    commands += MsgToDriverCabin.InformArrivingAtConnectingNode(connecting_node(route.last))

    commands.toList
  }

  private def update_route_graph() = {
    dcs.values foreach { dc =>
      route_graph.set_driver_cabin_location(dc, route_graph.get_dc_destination(dc))
    }
    gcs.values foreach { gc =>
      route_graph.set_guest_cabin_location(gc, route_graph.get_gc_destination(gc))
    }
  }
}

class MainServerHeart extends Actor {

  import MsgToHeart._

  var main_server_die = false

  println("[Main Server] Heart start moving")

  override def receive: Receive = {
    case Listen() => {
      if (!main_server_die) {
        sender ! MsgToCardiophone.HeartBeat()
      } else {
        // do nothing
      }

    }

    case Stop() => {
      main_server_die = true
    }

    case Reborn() => {
      main_server_die = false
    }
  }
}