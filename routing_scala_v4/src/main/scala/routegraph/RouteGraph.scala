package routegraph

import akka.actor.ActorRef
import scala.collection.mutable
import scala.io._

object RouteGraph {

  def file2route_graph(file_name: String): RouteGraph = {
    // return RouteGraph, center, routes

    def set_nodes(route_graph: RouteGraph, nodes: Set[String], area: String) = {
      for (node <- nodes) {
        route_graph.add(node, area)
      }
    }

    def set_edges(route_graph: RouteGraph, nodes: List[String]) = {
      var prev = nodes(0)
      for (node <- nodes.slice(1, nodes.size)) {
        route_graph.add((prev, node))
        prev = node
      }
    }

    val file = Source.fromFile(file_name)
    val route_graph = new RouteGraph()

    try {
      var area_loop = false
      var edge_loop = false

      file.getLines() foreach { line: String =>
        if (line != "") {
          // split line by spaces
          val tokens = line split """\s+"""
          // divide tokens to command and nodes
          val (command, nodes) = (tokens(0), tokens.slice(1, tokens.size).toList)
          command match {
            case "END" => {
              area_loop = false
              edge_loop = false
            }
            case "AREA" => area_loop = true
            case "EDGES" => edge_loop = true
            case area if area_loop => {
              set_nodes(route_graph, nodes.toSet, area)
            }
            case node if edge_loop => set_edges(route_graph, List(node) ++ nodes)

          }
        }
      }
    } finally {
      file.close()
    }

    route_graph
  }
}

class RouteGraph {
  val nodes = mutable.Set[String]()
  val connecting_nodes = mutable.Set[String]()
  val edges = mutable.Set[(String, String)]()
  val edge_weight = mutable.Map[(String, String), Double]()
  val nodes_in_areas = mutable.Map[String, mutable.Set[String]]()
  // driver cabin status
  val driver_cabin_location = mutable.Map[ActorRef, String]()
  val time_driver_cabin_arrival = mutable.Map[ActorRef, Long]()
  val time_driver_cabin_departure = mutable.Map[ActorRef, Long]()
  val dc_destination = mutable.Map[ActorRef, String]()
  // guest cabin status
  val guest_cabin_location = mutable.Map[ActorRef, String]()
  val time_guest_cabin_arrival = mutable.Map[ActorRef, Long]()
  val time_guest_cabin_departure = mutable.Map[ActorRef, Long]()
  val gc_destination = mutable.Map[ActorRef, String]()
  // cabin status
  val cabin_label = mutable.Map[ActorRef, String]()
  val cabin_is_around = mutable.Map[ActorRef, Boolean]()
  val cabin_on_service = mutable.Set[ActorRef]()

  // add node
  def add(node: String, area: String): Boolean = {
    nodes += node
    if (nodes_in_areas contains area) {
      nodes_in_areas(area) ++= mutable.Set[String](node)
    } else {
      nodes_in_areas += (area -> mutable.Set[String](node))
    }
    if (this.is_connecting_node(node)) connecting_nodes += node
    true
  }

  // add edge
  def add(edge: (String, String), weight: Double = 500, directed: Boolean = true): Boolean = {
    // 'directed' is true => directed edge
    val (node1, node2) = edge
    val reverse_edge = (node2, node1)
    if (!(nodes contains node1)) {
      println(s"Error: ${node1} is not existing.")
      false
    }
    else if (!(nodes contains node2)) {
      println(s"Error: ${node2} is not existing.")
      false
    } else {
      edges += edge
      edge_weight += (edge -> weight)
      if (!directed) {
        edges += reverse_edge
        edge_weight += (reverse_edge -> weight)
      }
      true
    }
  }

  def is_connecting_node(node: String) = {
    if (this.connecting_nodes contains node) {
      true
    } else {
      val areas_of_node = mutable.Set[String]()
      for ((area, nodes) <- this.nodes_in_areas) {
        if (nodes contains node) areas_of_node += area
      }
      if (1 < areas_of_node.size) true
      else false
    }
  }

  def get_all_nodes() = this.nodes.toSet

  def get_all_edges() = this.edges.toSet

  def get_neighbors(node: String) = {
    val neighbors = mutable.Set[String]()

    for ((node1, node2) <- this.edges) {
      if (node1 == node) neighbors += node2
    }

    neighbors.toSet
  }

  def get_connecting_nodes() = this.connecting_nodes.toSet

  def get_nodes_of_area(area: String) = this.nodes_in_areas(area).toSet

  def get_all_areas() = this.nodes_in_areas.keys.toSet

  def get_area_of_node(node: String) = {
    val areas = mutable.Set[String]()

    for ((area, nodes) <- this.nodes_in_areas) {
      if (nodes contains node) areas += area
    }

    areas.toSet
  }

  def set_driver_cabin_location(dc: ActorRef, node: String) = {
    driver_cabin_location += (dc -> node)
  }

  //  def set_driver_cabin_location(dc: ActorRef, begin: String, end: String) = {
  //    driver_cabin_location.remove(dc)
  //    driver_cabin_location += (dc -> (begin, end))
  //  }

  def get_driver_cabin_location(dc: ActorRef) = {
    driver_cabin_location(dc)
  }

  def get_all_driver_cabin_location() = {
    driver_cabin_location
  }

  def set_guest_cabin_location(gc: ActorRef, node: String) = {
    guest_cabin_location += (gc -> node)
  }

  //  def set_guest_cabin_location(gc: ActorRef, begin: String, end: String) = {
  //    guest_cabin_location.remove(gc)
  //    guest_cabin_location += (gc -> (begin, end))
  //  }

  def get_guest_cabin_location(gc: ActorRef) = {
    guest_cabin_location(gc)
  }

  def get_all_guest_cabin_location() = {
    guest_cabin_location
  }

  def get_all_cabin_location() = {
    (driver_cabin_location ++ guest_cabin_location).toMap
  }

  def get_driver_cabin_in_location(node: String) = {
    (driver_cabin_location filter {
      _._2 == node
    }).keys.toSet
  }

  def get_guest_cabin_in_location(node: String) = {
    (guest_cabin_location filter {
      _._2 == node
    }).keys.toSet
  }

  def get_all_cabin_in_location(node: String) = {
    this.get_driver_cabin_in_location(node) ++ this.get_guest_cabin_in_location(node)
  }

  def get_weight(node1: String, node2: String) = {
    val edge = (node1, node2)
    val reverse_edge = (node2, node1)

    if (edge_weight contains edge) {
      edge_weight(edge)
    } else if (edge_weight contains reverse_edge) {
      edge_weight(reverse_edge)
    } else {
      -1.0
    }
  }

  def set_time_driver_cabin_arrival(dc: ActorRef, time: Long) = {
    time_driver_cabin_arrival += (dc -> time)
  }

  def get_time_driver_cabin_arrival(dc: ActorRef) = {
    time_driver_cabin_arrival.getOrElse(dc, -1L)
  }

  def set_time_driver_cabin_departure(dc: ActorRef, time: Long) = {
    time_driver_cabin_departure += (dc -> time)
  }

  def get_time_driver_cabin_departure(dc: ActorRef) = {
    time_driver_cabin_departure.getOrElse(dc, -1L)
  }

  def set_time_guest_cabin_arrival(gc: ActorRef, time: Long) = {
    time_guest_cabin_arrival += (gc -> time)
  }

  def get_time_guest_cabin_arrival(gc: ActorRef) = {
    time_guest_cabin_arrival.getOrElse(gc, -1L)
  }

  def set_time_guest_cabin_departure(gc: ActorRef, time: Long) = {
    time_guest_cabin_departure += (gc -> time)
  }

  def get_time_guest_cabin_departure(gc: ActorRef) = {
    time_guest_cabin_departure.getOrElse(gc, -1L)
  }

  def set_cabin_label(cabin: ActorRef, label: String) = {
    cabin_label += (cabin -> label)
  }

  def get_cabin_label(cabin: ActorRef) = cabin_label.getOrElse(cabin, "")

  def get_cabin_labels() = cabin_label.toMap

  def set_dc_destination(dc: ActorRef, destination: String) = {
    dc_destination += (dc -> destination)
  }

  def set_gc_destination(gc: ActorRef, destination: String) = {
    gc_destination += (gc -> destination)
  }

  def get_dc_destination(dc: ActorRef) = dc_destination(dc)

  def get_gc_destination(gc: ActorRef) = gc_destination(gc)


  def cabin_can_change_label(cabin: ActorRef) = {
    cabin_is_around(cabin)
  }

  def set_cabin_on_service(cabin: ActorRef) = {
    cabin_on_service += cabin
  }

  def set_cabin_out_of_service(cabin: ActorRef) = {
    cabin_on_service remove cabin
  }

  def get_cabin_on_service(cabin: ActorRef) = {
    cabin_on_service contains cabin
  }

  //  def shortest_path(begin: String, end: String) = {
  //    // dijkstra
  //    val dest = mutable.Map[String, Double]()
  //    for (node <- get_all_nodes()) {
  //      if (node == begin)
  //        dest += (node -> 0)
  //      else
  //        dest += (node -> Double.MaxValue)
  //    }
  //
  //    val not_finish = mutable.Set(get_all_nodes().toArray:_*)
  //    var nNode = get_all_nodes().size
  //
  //    val prev = mutable.Map[String, String]()
  //    for (nd <- get_all_nodes()) {
  //      prev(nd) = ""
  //    }
  //
  //    while (not_finish.nonEmpty) {
  //      var nd = ""
  //      var d = Double.MaxValue
  //      for (n <- dest.keys) {
  //        if (not_finish contains n) {
  //          val dd = dest(n)
  //          if (dd < d) {
  //            nd = n
  //            d = dd
  //          }
  //        }
  //      }
  //      not_finish remove nd
  //
  //      var neighbor = ""
  //
  //      for (n <- get_neighbors(nd)) {
  //        if (dest(n) > dest(nd) + get_weight(nd, n)) {
  //          neighbor = n
  //          dest.update(n, dest(nd) + get_weight(nd, n))
  //        }
  //      }
  //      prev(neighbor) = nd
  //    }
  //
  //    var node = end
  //    val reverse_path = mutable.ListBuffer[String]()
  //
  //    reverse_path += node
  //    while(node != begin) {
  //      val p = prev(node)
  //      node = p
  //      reverse_path += node
  //    }
  //    reverse_path.reverse.toList
  //  }
}
