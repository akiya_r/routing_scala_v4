package main

import akka.actor.Actor.Receive
import akka.actor._
import message._
import routegraph.RouteGraph

import scala.util.Random

class RequestPublisher(seed: Int, rg: RouteGraph, ms: ActorRef) extends Actor {

  import MsgToRequestPublisher._

  val main_server = ms
  val route_graph = rg
  val nodes = route_graph.get_all_nodes().toList
  val max_nPeople = 10
  val rand = new Random(seed)

  override def receive: Receive = {
    case Publish() => {
      for (i <- 5 to 5 + rand.nextInt(10)) {

        val time_now = System.currentTimeMillis()

        val nPeople = 1 // rand.nextInt(max_nPeople)
        val time_earliest = time_now + rand.nextInt(30000)
        val time_latest = time_earliest + rand.nextInt(30000)
        val departure = nodes(rand.nextInt(nodes.size))
        val destination = nodes.filterNot(_ == departure)(rand.nextInt(nodes.size - 1))

        main_server ! MsgToMainServer.Request(nPeople, (time_earliest, time_latest), departure, destination)
      }
      Thread.sleep(1000 + rand.nextInt(20000))
      self ! Publish()
    }
  }
}
