package main

import java.awt.{BasicStroke, Font}
import java.awt.image.BufferedImage
import javax.swing.ImageIcon

import akka.actor._
import akka.pattern.ask
import akka.util.Timeout

import scala.concurrent.{Await, Future}
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._
import scala.language.postfixOps
import scala.util.{Failure, Random, Success, Try}
import java.util.concurrent.TimeoutException

import routegraph._
import server.{ConnectingNodeServer, MainServer}
import cabin.{DriverCabin, GuestCabin}
import message._

import scala.collection.mutable
import scala.concurrent.Await
import scala.io.Source
import scala.swing.Image

//object Main extends App {
//  implicit val timeout = Timeout(5.seconds)
//  val nDC = 2
//  val nGC = 1
//  val route_graph = RouteGraph.file2route_graph("001.route")
//  // test of route graph
//  println("nodes; " + route_graph.get_all_nodes())
//  println("edges; " + route_graph.get_all_edges())
//  println("c_nds; " + route_graph.get_connecting_nodes())
//  for (area <- route_graph.get_all_areas()) {
//    println(area + "; " + route_graph.get_nodes_of_area(area))
//  }
//
//  val system = ActorSystem("system")
//  val main_server = system.actorOf(Props(classOf[MainServer], nDC, nGC, route_graph), "main_server")
//  create_driver_cabin()
//  create_guest_cabin()
//
//
//  // ===== ===== ===== ===== ===== ===== ===== ===== ===== ===== ===== ===== ===== ===== ===== ===== ===== =====
//
//  def create_driver_cabin() = {
//    val dcs = mutable.Map[String, ActorRef]()
//    val future = system.actorSelection("/user/main_server/c1").resolveOne()
//    val node = Await.result(future, timeout.duration)
//
//    for (i <- 1 to nDC) {
//      val name = s"DriverCabin$i"
//      dcs += (name -> system.actorOf(Props(classOf[DriverCabin], node), name))
//    }
//    dcs.toMap
//  }
//
//  def create_guest_cabin() = {
//    val gcs = mutable.Map[String, ActorRef]()
//    val future = system.actorSelection("/user/main_server/c1").resolveOne()
//    val node = Await.result(future, timeout.duration)
//
//    for (i <- 1 to nGC) {
//      val name = s"GuestCabin$i"
//      gcs += (name -> system.actorOf(Props(classOf[GuestCabin], node), name))
//    }
//    gcs.toMap
//  }
//}

import swing._
import scala.swing.event._
import java.awt.{Color, Graphics2D, Point, geom, RenderingHints, Image}

import breeze.linalg._

object Main extends SimpleSwingApplication {

  import MsgToGraphics._


  val system = ActorSystem("system")

  lazy val ui = new Panel {
    // initialize ===== ===== ===== ===== ===== ===== ===== ===== ===== ===== ===== ===== ===== ===== ===== =====
    implicit val timeout = Timeout(5.seconds)
    val route_graph = RouteGraph.file2route_graph("002.route")
    // test of route graph
    val nodes = route_graph.get_all_nodes()
    val connecting_nodes = route_graph.get_connecting_nodes()
    val areas = route_graph.get_all_areas()
    println("nodes; " + route_graph.get_all_nodes())
    println("edges; " + route_graph.get_all_edges())
    println("c_nds; " + route_graph.get_connecting_nodes())
    for (area <- route_graph.get_all_areas()) {
      println(area + "; " + route_graph.get_nodes_of_area(area))
    }

    val nDC = 2
    val nGC = 2
    val main_server = system.actorOf(Props(classOf[MainServer], nDC, nGC, route_graph), "main_server")
    val c_node = init_connecting_node_servers()
    val driver_cabin = create_driver_cabin()
    val guest_cabin = create_guest_cabin()
    val request_publisher =
      system.actorOf(Props(classOf[RequestPublisher], 1381, route_graph, main_server), "publisher")

    request_publisher ! MsgToRequestPublisher.Publish()


    def init_connecting_node_servers() = {
      val servers = mutable.Map[String, ActorRef]()

      for (node <- route_graph.get_connecting_nodes()) {
        servers += (node -> system.actorOf(Props(classOf[ConnectingNodeServer], route_graph), node))
      }

      servers.toMap
    }

    def create_driver_cabin() = {
      val dcs = mutable.Map[String, ActorRef]()
      val future = system.actorSelection("/user/c1").resolveOne()
      val node = Await.result(future, timeout.duration)

      for (i <- 1 to nDC) {
        val name = s"DC$i"
        dcs += (name -> system.actorOf(Props(classOf[DriverCabin], node, route_graph), name))
      }
      dcs.toMap
    }

    def create_guest_cabin() = {
      val gcs = mutable.Map[String, ActorRef]()
      val future = system.actorSelection("/user/c1").resolveOne()
      val node = Await.result(future, timeout.duration)

      for (i <- 1 to nGC) {
        val name = s"GC$i"
        gcs += (name -> system.actorOf(Props(classOf[GuestCabin], node), name))
      }
      gcs.toMap
    }

    // ===== ===== ===== ===== ===== ===== ===== ===== ===== ===== ===== ===== ===== ===== ===== ===== ===== =====

    var width = 1300
    var height = 800
    val (connecting_node_width, connecting_node_height) = (20, 20)
    val node_radius = 20
    val area_color = (for (area <- areas) yield (area, new Color(area.hashCode().toInt))).toMap

    val driver_cabin_image =
      new ImageIcon("./img/driver_cabin.png").getImage.getScaledInstance(25, 25, Image.SCALE_SMOOTH)
    val guest_cabin_image =
      new ImageIcon("./img/guest_cabin.png").getImage.getScaledInstance(25, 25, Image.SCALE_SMOOTH)
    val driver_cabin_icon = driver_cabin_image.getScaledInstance(15, 15, Image.SCALE_SMOOTH)
    val guest_cabin_icon = guest_cabin_image.getScaledInstance(15, 15, Image.SCALE_SMOOTH)
    val user_icon = new ImageIcon("./img/user.png").getImage.getScaledInstance(15, 15, Image.SCALE_SMOOTH)

    background = new Color(200, 200, 200)
    preferredSize = new Dimension(width, height)
    focusable = true

    val node_position = file2position("002.pos")

    //    listenTo(mouse.clicks, mouse.moves, keys)
    //    reactions += {
    //      case e: MousePressed =>
    //        println(e.point)
    //        repaint()
    //      case e: MouseDragged =>
    //        println(e.point)
    //      case KeyTyped(_, 'l', _, _) =>
    //    }

    def file2position(f_name: String) = {
      val file = Source.fromFile(f_name)
      val node_position = mutable.Map[String, (Double, Double)]()

      try {
        var area_loop = false
        var edge_loop = false

        file.getLines() foreach { line: String =>
          val tokens = line.split("""\s+""")
          node_position += (tokens(0) -> (tokens(1).toDouble, tokens(2).toDouble))
        }
      } finally {
        file.close()
      }
      node_position
    }

    // ===== ===== ===== ===== ===== ===== ===== ===== ===== ===== ===== ===== ===== ===== ===== ===== ===== =====

    override def paintComponent(g: Graphics2D) = {
      super.paintComponent(g)

      val (dc_location, connection_gcs) = get_location()
      val cabin_label = get_label()
      val requests = get_request()

      g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON)

      val size = self.getSize()
      width = size.width
      height = size.height

      val af = new geom.AffineTransform()
      af.setToTranslation(width / 2, height / 2)
      g.setTransform(af)

      for ((nd1, nd2) <- route_graph.get_all_edges()) {
        draw_edge(g, nd1, nd2)
      }

      node_position.keys foreach {
        draw_node(g, _, dc_location, connection_gcs, requests)
      }

      draw_driver_cabins(g, dc_location, cabin_label)

      draw_guest_cabins(g, dc_location, connection_gcs, cabin_label)

      //      Thread.sleep(500)
      repaint()
    }

    def diag_vec(node1: String, node2: String) = {
      val r = 0.8
      val nd1 = DenseVector(node_position(node1)._1, node_position(node1)._2)
      val nd2 = DenseVector(node_position(node2)._1, node_position(node2)._2)
      val horizontal_vec = (nd2 - nd1) :* 0.5
      val vertical_vec = DenseVector((-horizontal_vec(1) * r).toInt, (horizontal_vec(0) * r).toInt)

      (horizontal_vec, vertical_vec)
    }

    def get_area_color(areas: Set[String]) = {
      var (red, green, blue) = (0, 0, 0)
      for (area <- areas) {
        red += area_color(area).getRed
        green += area_color(area).getGreen
        blue += area_color(area).getBlue
      }
      new Color(red / areas.size, green / areas.size, blue / areas.size)
    }

    def draw_edge(g: Graphics2D, node1: String, node2: String) = {
      // version 1
      //      val areas = route_graph.get_area_of_node(node1) & route_graph.get_area_of_node(node2)
      //      val color = get_area_color(areas)
      //      val nd1 = DenseVector(node_position(node1)._1, node_position(node1)._2)
      //      val nd2 = DenseVector(node_position(node2)._1, node_position(node2)._2)
      //      val (horizontal_vec, vertical_vec) = diag_vec(node1, node2)
      //      val middle_point = horizontal_vec + vertical_vec
      //
      //      g.setColor(new Color(color.getRed / 2, color.getGreen / 2, color.getBlue / 2))
      //      g.drawLine(nd1(0), nd1(1), middle_point(0), middle_point(1))
      val areas = route_graph.get_area_of_node(node1) & route_graph.get_area_of_node(node2)
      val color = get_area_color(areas)
      val nd1 = DenseVector(node_position(node1)._1, node_position(node1)._2)
      val nd2 = DenseVector(node_position(node2)._1, node_position(node2)._2)
      val (horizontal_vec, vertical_vec) = diag_vec(node1, node2)
      //      val middle_point = horizontal_vec + vertical_vec
      g.setStroke(new BasicStroke(4.0f))
      g.setColor(new Color(color.getRed, color.getGreen, color.getBlue, 100))
      //      g.drawLine(nd1(0), nd1(1), middle_point(0), middle_point(1))
      //      g.drawLine(middle_point(0), middle_point(1), nd2(0), nd2(1))
      g.drawLine(nd1(0).toInt, nd1(1).toInt, nd2(0).toInt, nd2(1).toInt)
      g.setStroke(new BasicStroke(1.0f))
    }

    def draw_node(g: Graphics2D, node: String,
                  dc_location: Map[String, Location],
                  connection_gcs: Map[String, Set[String]],
                  users: Map[String, Int]
                 ) = {
      val (x, y) = node_position(node)
      // calc middle color of area connecting node is belonged to
      val areas = route_graph.get_area_of_node(node)
      val color = get_area_color(areas)
      g.setColor(color)

      if (connecting_nodes contains node) {
        val draw_x = x.toInt - connecting_node_width / 2
        val draw_y = y.toInt - connecting_node_height / 2
        // node color
        g.fillRect(draw_x, draw_y, connecting_node_width, connecting_node_height)
        // display label
        g.drawString(node, x.toInt - connecting_node_width / 2, y.toInt - connecting_node_height / 2)
        // draw edge
        g.setColor(new Color(0, 0, 0))
        g.drawRect(draw_x, draw_y, connecting_node_width, connecting_node_height)
        // the number of dc and gc in this connecting node
        val iDC = dc_location.values.toList count { location: Location =>
          (location.prev == node) && location.next.isEmpty
        }
        val iGC = connection_gcs(node).size

        g.drawImage(driver_cabin_icon, x.toInt, y.toInt, null)
        g.drawImage(guest_cabin_icon, x.toInt, y.toInt + driver_cabin_icon.getHeight(null) + 5, null)
        g.drawImage(user_icon, x.toInt,
          y.toInt + driver_cabin_icon.getHeight(null) + guest_cabin_icon.getHeight(null) + 10, null)
        g.drawString(s"×$iDC", x.toInt + driver_cabin_icon.getWidth(null),
          y.toInt + driver_cabin_icon.getHeight(null))
        g.drawString(s"×$iGC", x.toInt + guest_cabin_icon.getWidth(null),
          y.toInt + driver_cabin_icon.getHeight(null) + guest_cabin_icon.getHeight(null) + 5)
        g.drawString(s"×${users.getOrElse(node, 0)}", x.toInt + user_icon.getWidth(null),
          y.toInt + driver_cabin_icon.getHeight(null) +
            guest_cabin_icon.getHeight(null) + user_icon.getHeight(null) + 10)

      } else {
        val draw_x = x.toInt - node_radius / 2
        val draw_y = y.toInt - node_radius / 2
        // draw node color
        g.fillOval(draw_x, draw_y, node_radius, node_radius)
        // display label
        g.drawString(node, draw_x, draw_y)
        // draw edge
        g.setColor(new Color(0, 0, 0))
        g.drawOval(draw_x, draw_y, node_radius, node_radius)
        // draw status
        g.drawImage(user_icon, x.toInt, y.toInt, null)
        g.drawString(s"×${users.getOrElse(node, 0)}",
          x.toInt + user_icon.getWidth(null), y.toInt + user_icon.getHeight(null))
      }

    }

    def draw_driver_cabins(g: Graphics2D,
                           cabin_location: Map[String, Location],
                           cabin_label: Map[String, String]) = {
      for ((dc, location) <- cabin_location) {
        if (0 < location.progress) {
          val (prev_x, prev_y) = node_position(location.prev)
          val (next_x, next_y) = node_position(location.next.get)
          val draw_x = (1 - location.progress) * prev_x + location.progress * next_x
          val draw_y = (1 - location.progress) * prev_y + location.progress * next_y
          g.setColor(new Color(0, 0, 0))
          g.drawImage(driver_cabin_image, draw_x.toInt, draw_y.toInt, null)
          g.drawString(dc, draw_x.toInt, draw_y.toInt)
          if (cabin_label contains dc) {
            val label = cabin_label(dc)
            g.setColor(get_area_color(Set(label)))
            g.fillRect(draw_x.toInt,
              draw_y.toInt + driver_cabin_image.getHeight(null) + 3,
              driver_cabin_image.getWidth(null),
              5)
          }
        }
      }
    }

    def draw_guest_cabins(g: Graphics2D,
                          dc_location: Map[String, Location],
                          connection_gcs: Map[String, Set[String]],
                          cabin_label: Map[String, String]) = {
      for ((connection, gcs) <- connection_gcs) {
        if (driver_cabin contains connection) {
          val location = dc_location(connection)
          if (0 < location.progress) {
            val (prev_x, prev_y) = node_position(location.prev)
            val (next_x, next_y) = node_position(location.next.get)
            val dc_x = (1 - location.progress) * prev_x + location.progress * next_x
            val draw_y = (1 - location.progress) * prev_y + location.progress * next_y
            for ((gc, i) <- gcs.zipWithIndex) {
              val draw_x = dc_x + (driver_cabin_image.getWidth(null) + 5) * (i + 1)
              val label = cabin_label(gc)
              // draw picture
              g.drawImage(guest_cabin_image, draw_x.toInt, draw_y.toInt, null)
              // draw name
              g.setColor(new Color(0, 0, 0))
              g.drawString(gc, draw_x.toInt, draw_y.toInt)
              // draw label
              g.setColor(get_area_color(Set(label)))
              g.fillRect(draw_x.toInt,
                draw_y.toInt + guest_cabin_image.getHeight(null) + 3,
                guest_cabin_image.getWidth(null),
                5)
            }
          }
        }
      }
    }


    def get_location() = {
      val dc_location_future = main_server ? MsgToMainServer.GetDriverCabinLocations()
      val Locations(dc_location: Map[String, Location]) =
        Await.result(dc_location_future, timeout.duration).asInstanceOf[Locations]

      val gc_location_future = main_server ? MsgToMainServer.GetGuestCabinLocations()
      val Connections(connection_gcs: Map[String, Set[String]]) =
        Await.result(gc_location_future, timeout.duration).asInstanceOf[Connections]

      (dc_location, connection_gcs)

    }

    def get_label() = {
      val future = main_server ? MsgToMainServer.GetLabels()
      val label = Await.result(future, timeout.duration).asInstanceOf[Map[String, String]]
      label
    }

    def get_request() = {
      val future = main_server ? MsgToMainServer.GetRequests()
      val node_user = Await.result(future, timeout.duration).asInstanceOf[Map[String, Int]]
      node_user
    }


  }

  def top = new MainFrame {
    title = "Simple Demo"
    contents = ui

    override def closeOperation(): Unit = {
      system.terminate()
    }
  }
}

object Main2 extends SimpleSwingApplication {

  import MsgToGraphics._

  implicit val timeout = Timeout(10.seconds)
  val system = ActorSystem("system")
  val rand = new Random(119)
  val route_file = "tsukuba_map"
  val init_node = "Tsukuba_Center"

  lazy val ui = new Panel {
    // initialize ===== ===== ===== ===== ===== ===== ===== ===== ===== ===== ===== ===== ===== ===== ===== =====

    val nDC = 4
    val nGC = 50
    var main_server_die = false
    val route_graph = RouteGraph.file2route_graph(s"${route_file}.route")
    // test of route graph
    val nodes = route_graph.get_all_nodes()
    val connecting_nodes = route_graph.get_connecting_nodes()
    val areas = route_graph.get_all_areas()
    println("nodes; " + route_graph.get_all_nodes())
    println("edges; " + route_graph.get_all_edges())
    println("c_nds; " + route_graph.get_connecting_nodes())
    for (area <- route_graph.get_all_areas()) {
      println(area + "; " + route_graph.get_nodes_of_area(area))
    }

    var main_server = system.actorOf(Props.empty)
    val c_node = mutable.Map[String, ActorRef]()
    val driver_cabin = mutable.Map[String, ActorRef]()
    val guest_cabin = mutable.Map[String, ActorRef]()

    def init_connecting_node_servers() = {
      val servers = mutable.Map[String, ActorRef]()

      for (node <- route_graph.get_connecting_nodes()) {
        servers += (node -> system.actorOf(Props(classOf[ConnectingNodeServer], route_graph), node))
      }

      servers.toMap
    }

    def create_driver_cabin() = {
      val dcs = mutable.Map[String, ActorRef]()
      val future = system.actorSelection(s"/user/${init_node}").resolveOne()
      val node = Await.result(future, timeout.duration)

      for (i <- 1 to nDC) {
        val name = s"DC$i"
        dcs += (name -> system.actorOf(Props(classOf[DriverCabin], node, route_graph), name))
      }
      dcs.toMap
    }

    def create_guest_cabin() = {
      val gcs = mutable.Map[String, ActorRef]()

      for (i <- 1 to nGC) {
        val c_node = route_graph.get_connecting_nodes().toList
        val node = c_node(rand.nextInt(c_node.size))
        val future = system.actorSelection(s"/user/$node").resolveOne()
        val node_ref = Await.result(future, timeout.duration)
        val name = s"GC$i"
        gcs += (name -> system.actorOf(Props(classOf[GuestCabin], node_ref), name))
      }
      gcs.toMap
    }

    // ===== ===== ===== ===== ===== ===== ===== ===== ===== ===== ===== ===== ===== ===== ===== ===== ===== =====

    var width = 1300
    var height = 800
    val (connecting_node_width, connecting_node_height) = (20, 20)
    val node_radius = 20
    val area_color = (for (area <- areas) yield (area, new Color(area.hashCode()))).toMap
    var grepped_node: Option[String] = None


    val driver_cabin_image =
      new ImageIcon("./img/driver_cabin.png").getImage.getScaledInstance(25, 25, Image.SCALE_SMOOTH)
    val guest_cabin_image =
      new ImageIcon("./img/guest_cabin.png").getImage.getScaledInstance(25, 25, Image.SCALE_SMOOTH)
    val driver_cabin_icon = driver_cabin_image.getScaledInstance(15, 15, Image.SCALE_SMOOTH)
    val guest_cabin_icon = guest_cabin_image.getScaledInstance(15, 15, Image.SCALE_SMOOTH)
    val user_icon = new ImageIcon("./img/user.png").getImage.getScaledInstance(15, 15, Image.SCALE_SMOOTH)


//    val color_main_server_live = new Color(200, 200, 200)
//    val color_main_server_die = new Color(220, 140, 160)

    val color_main_server_live = new Color(200, 200, 200)
    val color_main_server_die = new Color(200, 200, 200)
    background = color_main_server_live
    preferredSize = new Dimension(width, height)
    focusable = true

    val node_position = file2position(s"${route_file}.pos")
    node_position ++= add_position()

    def add_position() = {
      val rand = new Random(113)
      val additional_node_position = mutable.Map[String, (Double, Double)]()
      for (node <- route_graph.get_all_nodes()) {
        if (!(node_position contains node)) {
          val x = -width / 2 + width * rand.nextDouble() / 2
          val y = -height / 2 + height * rand.nextDouble() / 2

          additional_node_position += (node -> (x, y))
        }
      }
      additional_node_position
    }

    def file2position(f_name: String) = {
      import java.io._

      val node_position = mutable.Map[String, (Double, Double)]()

      try {
        val file = new File(f_name)
        val br = new BufferedReader(new FileReader(file))
        var line = br.readLine()
        while (line != null) {
          println(line)
          val tokens = line.split("""\s+""")
          node_position += (tokens(0) -> (tokens(1).toDouble, tokens(2).toDouble))
          line = br.readLine()
          //          line match {
          //            case "POSITION" => {
          //              var line2 = ""
          //              while ((line2 = br.readLine()) != "\n") {
          //                val tokens = line2.split("""\s+""")
          //                node_position += (tokens(0) -> (tokens(1).toDouble, tokens(2).toDouble))
          //              }
          //            }
          //            case "EDGES" => {
          //              var line2 = ""
          //              while ((line2 = br.readLine()) !="\n") {
          //                val tokens = line2.split("""\s+""")
          //                val
          //              }
          //            }
          //          }
        }
        br.close()
      } catch {
        case e: IOException => // do nothing
      }

      node_position
    }


    // ===== ===== ===== ===== ===== ===== ===== ===== ===== ===== ===== ===== ===== ===== ===== ===== ===== =====

    listenTo(mouse.clicks, mouse.moves, keys)
    reactions += {
      case e: MousePressed => {
        grepped_node = nearestNode(e.point)
      }
      case e: MouseDragged => {
        grepped_node match {
          case None => {
            // do nothing
          }
          case Some(node) => {
            set_new_pos(node, e.point)
            repaint()
          }
        }
      }
      // start simulating
      case KeyTyped(_, ' ', _, _) => {
        main_server = system.actorOf(Props(classOf[MainServer], nDC, nGC, route_graph), "main_server")
        c_node ++= init_connecting_node_servers()
        driver_cabin ++= create_driver_cabin()
        guest_cabin ++= create_guest_cabin()
        val request_publisher =
          system.actorOf(Props(classOf[RequestPublisher], 1381, route_graph, main_server), "publisher")

        request_publisher ! MsgToRequestPublisher.Publish()
        repaint()
      }

      case KeyTyped(_, 's', _, _) => {
        save_pos()
      }

      case KeyTyped(_, 'd', _, _) => {
        main_server ! MsgToMainServer.Die()
        main_server_die = true
        background = color_main_server_die
      }

      case KeyTyped(_, 'r', _, _) => {
        main_server ! MsgToMainServer.Reborn()
        main_server_die = false
        background = color_main_server_live
      }
    }

    def save_pos() = {
      import java.io.PrintWriter
      val file = new PrintWriter("output.pos")
      for ((name, pos) <- node_position) {
        file.write(s"$name ${pos._1} ${pos._2}\n")
      }
      file.close()
    }


    // ===== ===== ===== ===== ===== ===== ===== ===== ===== ===== ===== ===== ===== ===== ===== ===== ===== =====

    override def paintComponent(g: Graphics2D) = {
      super.paintComponent(g)

      val size = self.getSize()
      width = size.width
      height = size.height

      // rendering
      g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON)

      if (main_server_die) {
        g.setColor(new Color(220, 20, 60))
        g.fillRect(0, height - 30, width, 30)
        g.setColor(new Color(0, 0, 0))
        g.drawString("EMERGENCY :: Main Server is Down.", 10, height - 10)
      }


      val (dc_location, connection_gcs, cabin_label, requests) = if (main_server.path.name == "main_server") {
        (get_dc_location(), get_gc_connection(), get_label(), get_request())
      } else {
        (Map[String, Location](), Map[String, Set[String]](), Map[String, String](), Map[String, Int]())
      }

      val af = new geom.AffineTransform()
      af.setToTranslation(width / 2, height / 2)
      g.setTransform(af)

      for ((nd1, nd2) <- route_graph.get_all_edges()) {
        draw_edge(g, nd1, nd2)
      }

      node_position.keys foreach {
        draw_node(g, _, dc_location, connection_gcs, requests)
      }

      draw_driver_cabins(g, dc_location, cabin_label)

      draw_guest_cabins(g, dc_location, connection_gcs, cabin_label)

      //      Thread.sleep(500)
      repaint()
    }

    def diag_vec(node1: String, node2: String) = {
      val r = 0.8
      val nd1 = DenseVector(node_position(node1)._1, node_position(node1)._2)
      val nd2 = DenseVector(node_position(node2)._1, node_position(node2)._2)
      val horizontal_vec = (nd2 - nd1) :* 0.5
      val vertical_vec = DenseVector((-horizontal_vec(1) * r).toInt, (horizontal_vec(0) * r).toInt)

      (horizontal_vec, vertical_vec)
    }

    def get_area_color(areas: Set[String]) = {
      var (red, green, blue) = (0, 0, 0)
      for (area <- areas) {
        red += area_color.getOrElse(area, new Color(0, 0, 0)).getRed
        green += area_color.getOrElse(area, new Color(0, 0, 0)).getGreen
        blue += area_color.getOrElse(area, new Color(0, 0, 0)).getBlue
      }
      new Color(red / areas.size, green / areas.size, blue / areas.size)
    }

    def draw_edge(g: Graphics2D, node1: String, node2: String) = {
      // version 1
      //      val areas = route_graph.get_area_of_node(node1) & route_graph.get_area_of_node(node2)
      //      val color = get_area_color(areas)
      //      val nd1 = DenseVector(node_position(node1)._1, node_position(node1)._2)
      //      val nd2 = DenseVector(node_position(node2)._1, node_position(node2)._2)
      //      val (horizontal_vec, vertical_vec) = diag_vec(node1, node2)
      //      val middle_point = horizontal_vec + vertical_vec
      //
      //      g.setColor(new Color(color.getRed / 2, color.getGreen / 2, color.getBlue / 2))
      //      g.drawLine(nd1(0), nd1(1), middle_point(0), middle_point(1))
      val areas = route_graph.get_area_of_node(node1) & route_graph.get_area_of_node(node2)
      val color = get_area_color(areas)
      val nd1 = DenseVector(node_position(node1)._1, node_position(node1)._2)
      val nd2 = DenseVector(node_position(node2)._1, node_position(node2)._2)
      val (_, _) = diag_vec(node1, node2)
      //      val middle_point = horizontal_vec + vertical_vec
      val phi = if (nd1(0) < nd2(0)) {
        math.atan((nd1(1) - nd2(1)) / (nd1(0) - nd2(0))) + math.Pi
      } else {
        math.atan((nd1(1) - nd2(1)) / (nd1(0) - nd2(0)))
      }
      val sin30 = 15 * math.sin(math.Pi / 6)
      val cos30 = 15 * math.cos(math.Pi / 6)
      val arrow_pos1 = (cos30 * math.cos(phi) - sin30 * math.sin(phi), cos30 * math.sin(phi) + sin30 * math.cos(phi))
      val arrow_pos2 = (cos30 * math.cos(phi) + sin30 * math.sin(phi), cos30 * math.sin(phi) - sin30 * math.cos(phi))


      g.setStroke(new BasicStroke(4.0f))
      g.setColor(new Color(color.getRed, color.getGreen, color.getBlue, 100))
      //      g.drawLine(nd1(0), nd1(1), middle_point(0), middle_point(1))
      //      g.drawLine(middle_point(0), middle_point(1), nd2(0), nd2(1))
      g.drawLine(nd1(0).toInt, nd1(1).toInt, nd2(0).toInt, nd2(1).toInt)

      g.drawLine((nd1(0) + nd2(0)).toInt / 2, (nd1(1) + nd2(1)).toInt / 2, ((nd1(0) + nd2(0)).toInt / 2 + arrow_pos1._1).toInt, ((nd1(1) + nd2(1)).toInt / 2 + arrow_pos1._2).toInt)
      g.drawLine((nd1(0) + nd2(0)).toInt / 2, (nd1(1) + nd2(1)).toInt / 2, ((nd1(0) + nd2(0)).toInt / 2 + arrow_pos2._1).toInt, ((nd1(1) + nd2(1)).toInt / 2 + arrow_pos2._2).toInt)

      g.setStroke(new BasicStroke(1.0f))
    }

    def draw_node(g: Graphics2D, node: String,
                  dc_location: Map[String, Location],
                  connection_gcs: Map[String, Set[String]],
                  users: Map[String, Int]
                 ) = {
      val (x, y) = node_position(node)
      // calc middle color of area connecting node is belonged to
      val areas = route_graph.get_area_of_node(node)
      val color = get_area_color(areas)
      g.setColor(color)

      if (connecting_nodes contains node) {
        val draw_x = x.toInt - connecting_node_width / 2
        val draw_y = y.toInt - connecting_node_height / 2
        // node color
        g.fillRect(draw_x, draw_y, connecting_node_width, connecting_node_height)
        // display label
        g.drawString(node, x.toInt - connecting_node_width / 2, y.toInt - connecting_node_height / 2)
        // draw edge
        g.setColor(new Color(0, 0, 0))
        g.drawRect(draw_x, draw_y, connecting_node_width, connecting_node_height)
        // the number of dc and gc in this connecting node
        val iDC = dc_location.values.toList count { location: Location =>
          (location.prev == node) && location.next.isEmpty
        }
        val iGC = connection_gcs.getOrElse(node, Set[String]()).size

        g.drawImage(driver_cabin_icon, x.toInt, y.toInt, null)
        g.drawImage(guest_cabin_icon, x.toInt, y.toInt + driver_cabin_icon.getHeight(null) + 5, null)
        g.drawImage(user_icon, x.toInt,
          y.toInt + driver_cabin_icon.getHeight(null) + guest_cabin_icon.getHeight(null) + 10, null)
        g.drawString(s"×$iDC", x.toInt + driver_cabin_icon.getWidth(null),
          y.toInt + driver_cabin_icon.getHeight(null))
        g.drawString(s"×$iGC", x.toInt + guest_cabin_icon.getWidth(null),
          y.toInt + driver_cabin_icon.getHeight(null) + guest_cabin_icon.getHeight(null) + 5)
        g.drawString(s"×${users.getOrElse(node, 0)}", x.toInt + user_icon.getWidth(null),
          y.toInt + driver_cabin_icon.getHeight(null) +
            guest_cabin_icon.getHeight(null) + user_icon.getHeight(null) + 10)

      } else {
        val draw_x = x.toInt - node_radius / 2
        val draw_y = y.toInt - node_radius / 2
        // draw node color
        g.fillOval(draw_x, draw_y, node_radius, node_radius)
        // display label
        g.drawString(node, draw_x, draw_y)
        // draw edge
        g.setColor(new Color(0, 0, 0))
        g.drawOval(draw_x, draw_y, node_radius, node_radius)
        // draw status
        g.drawImage(user_icon, x.toInt, y.toInt, null)
        g.drawString(s"×${users.getOrElse(node, 0)}",
          x.toInt + user_icon.getWidth(null), y.toInt + user_icon.getHeight(null))
      }

    }

    def draw_driver_cabins(g: Graphics2D,
                           cabin_location: Map[String, Location],
                           cabin_label: Map[String, String]) = {
      for ((dc, location) <- cabin_location) {
        if (0 < location.progress) {
          val (prev_x, prev_y) = node_position(location.prev)
          val (next_x, next_y) = node_position(location.next.get)
          val draw_x = (1 - location.progress) * prev_x + location.progress * next_x
          val draw_y = (1 - location.progress) * prev_y + location.progress * next_y
          g.setColor(new Color(0, 0, 0))
          g.drawImage(driver_cabin_image, draw_x.toInt, draw_y.toInt, null)
          g.drawString(dc, draw_x.toInt, draw_y.toInt)
          if (cabin_label contains dc) {
            val label = cabin_label(dc)
            g.setColor(get_area_color(Set(label)))
            g.fillRect(draw_x.toInt,
              draw_y.toInt + driver_cabin_image.getHeight(null) + 3,
              driver_cabin_image.getWidth(null),
              5)
          }
        }
      }
    }

    def draw_guest_cabins(g: Graphics2D,
                          dc_location: Map[String, Location],
                          connection_gcs: Map[String, Set[String]],
                          cabin_label: Map[String, String]) = {
      for ((connection, gcs) <- connection_gcs) {
        if ((driver_cabin contains connection) && (dc_location contains connection)) {
          val location = dc_location(connection)
          if (0 < location.progress) {
            val (prev_x, prev_y) = node_position(location.prev)
            val (next_x, next_y) = node_position(location.next.get)
            val dc_x = (1 - location.progress) * prev_x + location.progress * next_x
            val draw_y = (1 - location.progress) * prev_y + location.progress * next_y
            for ((gc, i) <- gcs.zipWithIndex) {
              val draw_x = dc_x + (driver_cabin_image.getWidth(null) + 5) * (i + 1)
              val label = cabin_label.getOrElse(gc, "")
              // draw picture
              g.drawImage(guest_cabin_image, draw_x.toInt, draw_y.toInt, null)
              // draw name
              g.setColor(new Color(0, 0, 0))
              g.drawString(gc, draw_x.toInt, draw_y.toInt)
              // draw label
              g.setColor(get_area_color(Set(label)))
              g.fillRect(draw_x.toInt,
                draw_y.toInt + guest_cabin_image.getHeight(null) + 3,
                guest_cabin_image.getWidth(null),
                5)
            }
          }
        }
      }
    }


    def get_dc_location(): Map[String, Location] = {
      val dc_location_future = main_server ? MsgToMainServer.GetDriverCabinLocations()
      Await.result(dc_location_future, timeout.duration).asInstanceOf[Locations].locations
    }

    def get_gc_connection(): Map[String, Set[String]] = {
      val gc_location_future = main_server ? MsgToMainServer.GetGuestCabinLocations()
      Await.result(gc_location_future, timeout.duration).asInstanceOf[Connections].connection_gcs
    }

    def get_label(): Map[String, String] = {
      val future = main_server ? MsgToMainServer.GetLabels()
      Await.result(future, timeout.duration).asInstanceOf[Map[String, String]]
    }

    def get_request(): Map[String, Int] = {
      val future = main_server ? MsgToMainServer.GetRequests()
      Await.result(future, timeout.duration).asInstanceOf[Map[String, Int]]
    }

    def nearestNode(p: Point) = {
      var min_dist = Double.PositiveInfinity
      var nearest_node: Option[String] = None
      for (node <- route_graph.get_all_nodes()) {
        val (x, y) = node_position(node)
        val d = norm(p, new Point(x.toInt + width / 2, y.toInt + height / 2))
        if ((d < node_radius) && (d < min_dist)) {
          min_dist = d
          nearest_node = Some(node)
        }
      }
      nearest_node
    }

    def norm(p1: Point, p2: Point): Double = {
      import scala.math._
      sqrt(pow(p1.x - p2.x, 2) + pow(p1.y - p2.y, 2))
    }

    def set_new_pos(node: String, p: Point) = {
      val (x, y): (Double, Double) = (p.x - width / 2, p.y - height / 2)
      node_position += (node -> (x, y))
    }
  }

  def top = new MainFrame {
    title = "Simple Demo"
    contents = ui

    override def closeOperation(): Unit = {
      system.terminate()
    }
  }
}
