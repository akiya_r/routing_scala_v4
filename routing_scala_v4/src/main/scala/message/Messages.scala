package message

import akka.actor._
import routegraph.RouteGraph

object MsgToGraphics {

  case class Locations(locations: Map[String, Location])

  case class Location(prev: String, next: Option[String], progress: Double)

  case class Connections(connection_gcs: Map[String, Set[String]])

}

object MsgToRequestPublisher {

  case class Publish()

}

object MsgToMainServer {

  // from connecting node servers
  case class CabinsArriveAt(dc: ActorRef, gcs: Set[ActorRef], node: ActorRef)

  case class DriverCabinLeaveAndGoTo(dc: ActorRef, gcs: Set[ActorRef], node: ActorRef, route: List[String])

  case class BootDriverCabinAt(dc: ActorRef, node: ActorRef)

  case class BootGuestCabinAt(gc: ActorRef, node: ActorRef)

  case class BootConnectingNodeServer(node: ActorRef)

  // from cardiophone
  case class GetHeart()


  // from scheduler
  case class GetData()

  case class SchedulingResult(cabin_commands: Map[ActorRef, List[Any]])

  // cabins
  case class CabinChangeLabel(cabin: ActorRef, new_label: String)

  // the other
  case class GetDriverCabinLocations()

  case class GetGuestCabinLocations()

  case class GetLabels()

  case class GetRequests()

  case class Request(nPeople: Int, time_window: (Long, Long), departure: String, destination: String)

  case class Die()

  case class Reborn()

}

object MsgToScheduler {

  case class Data(route_graph: RouteGraph, dcs: Map[String, ActorRef], gcs: Map[String, ActorRef], c_node: Map[String, ActorRef])

  case class Scheduling()

  case class CollectData()

  case class Stop()

  // when the main server is stopped

}

object MsgToHeart {

  // from connecting node server
  case class Listen()

  // from main server
  case class Stop()

  // when the main server is stopped

  case class Reborn()

  // when the main server is reborn

}

object MsgToCardiophone {

  // from main server
  case class HeartBeat()

  case class NoHeartBeat()

  // from self
  case class NextCheck()

  case class FindMainServer()

  // from connecting node
  case class NewMainServerHeart(heart: ActorRef)

}


object MsgToConnectingNodeServer {

  // from driver cabin
  case class BootDriverCabin(dc: ActorRef)

  case class BootGuestCabin(gc: ActorRef)

  case class DriverCabinArrive(dc: ActorRef, gcs: Set[ActorRef])

  case class DriverCabinLeaveWith(dc: ActorRef, gcs: Set[ActorRef])

  // from main server
  case class CommandsTo(cabin: ActorRef, cmds: List[Any])

  case class StaticSchedule(route_nCabin: Map[List[String], (Int, Int)])

  // map(route, (iDc, iGc)


  // from cardiophone
  case class MainServerDie()

  case class MainServerReborn(new_main_server: ActorRef)

  // from self


}

object MsgToDriverCabin {

  case class Commands(commands: List[Any])

  case class ConnectGuestCabins(gcs: Set[ActorRef])

  case class DisconnectGuestCabins()

  case class GoTo(node: String)

  case class InformArrivingAtConnectingNode(node: ActorRef)

  case class InformLeavingAtConnectingNode(node: ActorRef)

  case class Wait(millisec: Long)

  case class SetLabel(label: String, cabin: ActorRef)

  case class GuestCabinIsReady()

  case class MainServerIsDead()

}

object MsgToGuestCabin {

  case class SetLabel(label: String)

  case class ConnectTo(dc: ActorRef)

  case class Disconnect()

  case class GoTo(node: String)

  case class ArriveAt(node: String)

  case class ArriveAtConnectingNode(node: ActorRef)

}