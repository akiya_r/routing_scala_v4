
name := "routing_scala_v4"

version := "1.0"

scalaVersion := "2.11.8"

libraryDependencies ++= Seq(
  "com.typesafe.akka" % "akka-actor_2.11" % "2.4.0",
  "org.scala-lang" % "scala-swing" % "2.11+",
  "org.scala-graph" %% "graph-core" % "1.11.2",
  "org.scalanlp" %% "breeze" % "0.12"
)

mainClass in assembly := Some("main.Main")
